package stack


import fmt "fmt"

type Stack struct {
	i int
	 data [10]int
	 name string
}


func (s *Stack) Push(k int){
      s.data[s.i]=k;
      s.i++;
}

func (s *Stack) Pop()(ret int){
	s.i--
	ret=s.data[s.i]
	return
}


func main()  {
        c :=new(Stack)
	c.Push(5)

	fmt.Printf("Hello World!%d \n ",c.Pop())
}